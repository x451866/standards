# Implementation of https://link.springer.com/chapter/10.1007/3-540-48970-3_24
from sage.all import randint, Integers,ln, ZZ, EllipticCurve, GF, kronecker

#Currently for d = 3
def condition_1(u,v,d):
    if u%3!=1 or v%3!=0:
        return 0
    p4 = u**2+3*v**2
    if p4%4!=0:
        return 0
    p = p4//4
    if p%3!=1 or p%4!=3:
        return 0
    if not p.is_prime():
        return 0
    return p,p+1+u

#Currently for d=8
def condition_2(u,v,d):
    p = u**2+2*v**2
    if p%4!=3:
        return 0
    if p%16==3:
        if not (u%4==1 and p.is_prime()):
            return 0
        return p,p+1-2*u
    if p%16==11:
        if not (u%4==3 and p.is_prime()):
            return 0
        return p,p+1-2*u
    return 0

#Currently for d = 7,11,19,43,67,163
def condition_3(u,v,d):
    if u==1:
        return 0
    p4 = u**2+d*v**2
    if p4%4!=0:
        return 0
    p = p4//4
    if not (p%4==3 and p.is_prime()):
        return 0
    s = kronecker(2*u,d)
    return p,p+1-s*u


models = {3:[0,16,condition_1],8:[-270,-1512,condition_2],7:[-35,-98, condition_3],11:[-9504,365904, condition_3],19:[-608,5776, condition_3],43:[-13760,621264, condition_3],67:[-117920,15585808, condition_3],163:[-34790720,78984748304, condition_3] }


def embedding_degree(p,r):
    return Integers(r)(p).multiplicative_order()

#Implemented without base-point generation
def lenstra_check(d: ZZ,u: ZZ,v: ZZ):
    result = {'fail':True}
    a,b,model = models[d]
    c = model(u,v,d)
    if c==0:
        return result
    p,n = c
    cofactor = {8:2,7:8}.get(d,1)
    if n%cofactor!=0:
        return result
    r = n//cofactor
    if not r.is_prime():
        return result
    emb = embedding_degree(p,r)
    if emb*(ln(emb*ln(p)))**2<=0.02*(ln(p))**2:
        return result
    result['fail'] = False
    result['curve'] = [d,p,r,a,b,cofactor]
    return result

def generate_lenstra(bits):
    while True:
        u = randint(2**(bits-1),2**bits-1)
        v0 =randint(2**(bits-1),2**bits-1)
        for i in range(256):
            v = v0+i
            for d in models:
                res = lenstra_check(ZZ(d),ZZ(u),ZZ(v))
                if not res['fail']:
                    d,p,r,a,b,h = res['curve']
                    E = EllipticCurve(GF(p),[a,b])
                    return E,r,h,d,u,v

def test():
    for _ in range(10):
        bits= 30
        E,r,h,d,u,v = generate_lenstra(bits)
        n = E.order()
        assert n%h==0
        assert n//h==r
        assert r.is_prime()
        t = E.trace_of_frobenius()
        D = t**2-4*E.base_field().order()
        cmD = D.squarefree_part()
        cmD = 4*cmD if cmD%4!=1 else cmD
        assert cmD==-d

if __name__=='__main__':
    test()
    print("ok")
