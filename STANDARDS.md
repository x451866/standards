STANDARDS



X962

- cofactor 1,2,4
- embedding degree > 20
- trace != 1
- a = -3

Brainpool

- cofactor 1
- embedding degree> (n-1)/100
- trace !=1
- CM > 2^100

NUMS

- cofactor 1
- twist cofactor 1
- embedding degree> (n-1)/100
- trace>1
- CM > 2^100

Curve25519

- cofactor  8
- twist cofactor 4
- trace != 0,1
- embedding degree > (n-1)/100
- CM>2^100

Random

- cofactor 1,2,4,8
- CM > 2^100
- embedding degree > (n-1)/100
- trace !=0,1

BN_SIM

