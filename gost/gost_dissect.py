from sage.all import *


Bs = [649, 759, 824, 827, 862, 871, 2532, 2572, 2716, 3132, 3254, 3385, 3899, 4086, 4286, 4335, 4440, 4569, 4621, 4631, 4964, 5766, 6057, 7478, 7849, 8453, 10946, 11081, 11482, 11671, 11896, 12096, 14006, 14285, 14981, 15340, 15612, 16749, 16975, 17134, 17573, 17790, 18019, 19008, 19124, 19447, 19633, 19757, 19943, 20129, 20475, 20948, 22360, 23193, 23268, 24626, 25838, 26159, 26303, 26474, 26943, 27175, 27264, 27309, 27421, 27433, 27532, 28102, 28190, 28419, 28921, 29306, 29562, 29793, 29946, 30629, 30871, 31403, 31592, 32858]

p = ZZ(70390085352083305199547718019018437841079516630045180471284346843705633502619)
F = GF(p)

a = ZZ(-3)

curves = []

for i,b in enumerate(Bs):
    print(i,b)
    E = EllipticCurve(F,[a,b])
    o = E.order()
    t = p+1-o
    D = t**2-4*p
    d = D.squarefree_part()
    if d%4!=1:
        d*=4
    e = Integers(o)(p).__pari__().znorder(o-1)
    j = E.j_invariant()

    curves.append({"name":f"fakegost{b}", "category":"fakegost_sim","field":{"type": "Prime", "p":hex(p),"bits":p.nbits()},"form": "Weierstrass", "params": {"a": {"raw": hex(a)}, "b": {"raw": hex(b)}}, "order":o, "cofactor":1, "properties": {"cm_discriminant": hex(d), "embedding_degree": hex(e), "trace": hex(t), "j_invariant": hex(j)} })

import json
with open("fakegost.json","w") as f:
    json.dump({"curves":curves},f)
