from sage.all import *


#paramset A
A = {"p": 115792089237316195423570985008687907853269984665640564039457584007913129639319,
"a" : -3,
"b" : 1}

C = {"p":70390085352083305199547718019018437841079516630045180471284346843705633502619, "a":-3,"b":1}

def check_conditions(curve):
    order = ZZ(curve.__pari__().ellsea(1))
    if order==0:
        return False
    if not order.is_prime():
        return False
    j = curve.j_invariant()
    if j in [0,1728]:
        return False
    p = curve.base_field().order()
    if p==order:
        return False

    em = Integers(order)(p).__pari__().znorder(p-1)
    if em<32:
        return False
    return True

import time
def find_curve(params):
    b = params["b"]
    a = params["a"]
    p = params["p"]
    start = time.time()
    while True:
        if b%200==0:
            end = time.time()
            print(end-start)
            start = end
        print(b)
        try:
            curve = EllipticCurve(GF(p),[a,b])
        except:
            b+=1
            continue
        if check_conditions(curve):
            print("\n success", b)
            break
        b+=1

if __name__=="__main__":
    #print("A")
    #find_curve(A)
    print("C")
    find_curve(C)
